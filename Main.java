import DB.AlunoDB;
import Modelos.Aluno;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
    static AlunoDB alunoDB = new AlunoDB();

    public static void main(String[] args) throws Exception {
        System.out.println("CHAMADA");
        int option;

        do{
            System.out.println("1 - Cadastrar presença");
            System.out.println("2 - Lista de presença");
            System.out.println("0 - Sair");

            System.out.print("Selecione uma opção: ");
            Scanner entrada = new Scanner(System.in);
            option = entrada.nextInt();

            process(option);

        } while (option != 0);
    }

    public static void process(int option) throws Exception{
        switch (option){
            case 1: {
                Scanner entrada = new Scanner(System.in);

                System.out.print("Nome do aluno: ");
                String nome = entrada.next();

                System.out.print("Aluno presente ou ausente: ");
                String status = entrada.next();

                System.out.print("Data: ");
                String dataString = entrada.next();

                Date dataPresenca = new SimpleDateFormat("dd/MM/yyyy").parse(dataString);

                Aluno novoAluno = new Aluno(nome, status, dataPresenca);
                alunoDB.addNovoAluno(novoAluno);

                break;
            }
            case 2: {
                List<Aluno> relatorioAluno = alunoDB.getAlunoList();

                for(Aluno aluno: relatorioAluno){
                    System.out.println("> Aluno: " + aluno.getNome());
                    System.out.println("> Status: " + aluno.getStatus());
                    System.out.println(("> Data: " + aluno.getDataPresenca()));
                    System.out.println("---------------------------------");
                }
            }
            break;
        }
    }
}
