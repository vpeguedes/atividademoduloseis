package DB;

import Modelos.Aluno;
import java.util.List;
import java.util.ArrayList;

public class AlunoDB {
    private List<Aluno> alunoList = new ArrayList<>();

    public List<Aluno> getAlunoList() {
        return alunoList;
    }

    public void addNovoAluno(Aluno aluno) {
        alunoList.add(aluno);
    }
}