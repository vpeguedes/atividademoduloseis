package Modelos;

import java.util.Date;

public class Aluno {
    private String nome;
    private String status;
    private Date dataPresenca;

    public Aluno(String nome, String status, Date dataPresenca){
    this.nome = nome;
    this.status = status;
    this.dataPresenca = dataPresenca;
    }

    public String getNome() {
        return nome;
    }

    public String getStatus() {
        return status;
    }

    public Date getDataPresenca() {
        return dataPresenca;
    }
}